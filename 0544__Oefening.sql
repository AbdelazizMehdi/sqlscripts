USE ModernWays;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Totaal aantal beluisteringen'
FROM Liedjes
WHERE Length(Artiest) >= 10
GROUP BY Artiest;